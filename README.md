Requirements:
- Truffle >=0.4.22 <0.9.0
- Node

**Preparation of the environment:**
1. Install node modules

    `npm install`

2. connect MetaMask to kovan network

3. run the project
 
    `npm run start`

