const DaiToken = artifacts.require('DaiToken')
const NovaToken = artifacts.require('NovaToken')
const NovaFarm = artifacts.require('NovaFarm')


function tokens(n) {
  return web3.utils.toWei(n, 'ether');
}

contract('NovaFarm', ([owner, investor]) => {
  let daiToken, novaToken, novaFarm

  before(async () => {
    daiToken = await DaiToken.new()
    novaToken = await NovaToken.new()
    novaFarm = await NovaFarm.new(novaToken.address, daiToken.address)

    await novaToken.transfer(novaFarm.address, tokens('1000000'))

    await daiToken.transfer(investor, tokens('100'), { from: owner })
  })

  describe('mDai deployment', async () => {
    it('has a name', async () => {
      const name = await daiToken.name()
      assert.equal(name, 'mDai token')
    })
  })

  describe('Nova Token deployment', async () => {
    it('has a name', async () => {
      const name = await novaToken.name()
      assert.equal(name, 'Nova Token')
    })
  })

  describe('Token Farm deployment', async () => {
    it('has a name', async () => {
      const name = await novaFarm.name()
      assert.equal(name, 'NOVA Token Farm')
    })

    it('contract has tokens', async () => {
      let balance = await novaToken.balanceOf(novaFarm.address)
      assert.equal(balance.toString(), tokens('1000000'))
    })
  })

  describe('Farming tokens', async () => {

    it('rewards investors for staking mDai tokens', async () => {
      let result

      result = await daiToken.balanceOf(investor)
      assert.equal(result.toString(), tokens('100'), 'investor Mock DAI wallet balance correct before staking')

      await daiToken.approve(novaFarm.address, tokens('100'), { from: investor })
      await novaFarm.stakeTokens(tokens('100'), { from: investor })

      result = await daiToken.balanceOf(investor)
      assert.equal(result.toString(), tokens('0'), 'investor Mock DAI wallet balance correct after staking')

      result = await daiToken.balanceOf(novaFarm.address)
      assert.equal(result.toString(), tokens('100'), 'Token Farm Mock DAI balance correct after staking')

      result = await novaFarm.stakingBalance(investor)
      assert.equal(result.toString(), tokens('100'), 'investor staking balance correct after staking')

      result = await novaFarm.isStaking(investor)
      assert.equal(result.toString(), 'true', 'investor staking status correct after staking')

      await novaFarm.issueTokens({ from: owner })

      result = await novaToken.balanceOf(investor)
      assert.equal(result.toString(), tokens('100'), 'investor DApp Token wallet balance correct affter issuance')


      await novaFarm.unstakeTokens({ from: investor })

      result = await daiToken.balanceOf(investor)
      assert.equal(result.toString(), tokens('100'), 'investor Mock DAI wallet balance correct after staking')

      result = await daiToken.balanceOf(novaFarm.address)
      assert.equal(result.toString(), tokens('0'), 'Token Farm Mock DAI balance correct after staking')

      result = await novaFarm.stakingBalance(investor)
      assert.equal(result.toString(), tokens('0'), 'investor staking balance correct after staking')

      result = await novaFarm.isStaking(investor)
      assert.equal(result.toString(), 'false', 'investor staking status correct after staking')
    })
  })

})
