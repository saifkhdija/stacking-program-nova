require('babel-register');
require('babel-polyfill');
const HDWalletProvider = require("@truffle/hdwallet-provider");
const MNEMONIC = 'ac5919846faa2648022b1ab12c16ea9352cf435aa8dfbf1c53dcf90078c53c4a';
const endpointUrl = "https://kovan.infura.io/v3/d2bd3f13b3504eaa9db50f5b61c76e71";
module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*" // Match any network id
    },
    kovan: {
      provider: () => new HDWalletProvider(MNEMONIC, endpointUrl),
      network_id: 42,       
      confirmations: 5, 
      timeoutBlocks: 200,
      skipDryRun: true 
    }
  },
  contracts_directory: './src/contracts/',
  contracts_build_directory: './src/abis/',
  compilers: {
    solc: {
      optimizer: {
        enabled: true,
        runs: 200
      },
      evmVersion: "petersburg"
    }
  }
}
