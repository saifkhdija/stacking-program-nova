const novaFarm = artifacts.require('NovaFarm')

module.exports = async function(callback) {
  const farm = await novaFarm.at('0x9Ce545377A3Cb45F68Fe8e14e0005E7A14F02B77');
  await farm.issueTokens()
  console.log("Tokens issued!")
  callback()
}
