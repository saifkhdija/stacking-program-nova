const NovaToken = artifacts.require('NovaToken')
const DaiToken = artifacts.require('DaiToken')
const NovaFarm = artifacts.require("NovaFarm");
const EthSwap = artifacts.require("EthSwap");
module.exports = async function(deployer, network, accounts) {
  await deployer.deploy(DaiToken)
  const daiToken = await DaiToken.deployed()

  await deployer.deploy(NovaToken)
  const novaToken = await NovaToken.deployed()

  await deployer.deploy(NovaFarm, novaToken.address, daiToken.address)
  const novaFarm = await NovaFarm.deployed()
  await deployer.deploy(EthSwap, daiToken.address);
  const ethSwap = await EthSwap.deployed()
  await daiToken.transfer(ethSwap.address, '100000000000000000000')
  await daiToken.transfer(daiToken.address, '100000000000000000000')
  await novaToken.transfer(novaFarm.address, '100000000000000000000')
}
