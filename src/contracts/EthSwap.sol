pragma solidity >=0.4.22 <0.9.0;

import "./DaiToken.sol";

contract EthSwap {
  string public name = "EthSwap Nova";
  DaiToken public token;
  address payable owner;
  uint public rate = 100;

  event buy(
    address account,
    address token,
    uint amount,
    uint rate
  );

  event sell(
    address account,
    address token,
    uint amount,
    uint rate
  );

  constructor(DaiToken _token) public {
    token = _token;
  }

  function buyTokens(uint _amount) public payable {
    uint tokenAmount = _amount * rate;
    require(token.balanceOf(address(this)) >= tokenAmount);
    token.transfer(msg.sender, tokenAmount);
    emit buy(msg.sender, address(token), tokenAmount, rate);
  }

  function sellTokens(uint _amount) public payable {
    require(token.balanceOf(msg.sender) >= _amount);
    uint etherAmount = _amount / rate;
    require(address(this).balance >= etherAmount);
    token.transferFrom(msg.sender, address(this), _amount);
    emit sell(msg.sender, address(token), _amount, rate);
  }

}
