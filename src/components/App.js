import React, { Component } from 'react'
import Web3 from 'web3'
import DaiToken from '../abis/DaiToken.json'
import NovaToken from '../abis/NovaToken.json'
import NovaFarm from '../abis/NovaFarm.json'
import EthSwap from '../abis/EthSwap.json'
import Navbar from './Navbar'
import Main from './Main'
import './App.css'

class App extends Component {
    async componentWillMount() {
    await this.loadWeb3()
    await this.loadBlockchainData()
    
  }
  
  async loadBlockchainData() {
    
    const web3 = window.web3
    const accounts = await web3.eth.getAccounts()
    const Dai_adress = "0xd75c397c5de3802284b7795b7Ce0349c8DBEE191"
    const nova_adress = "0x5aE0f4FDe91e06A69420fA885597f7990167bB9B"
    const farm_adress = "0x3D4f4270388d1a02937AD77622223FC5bA72d0fB"
    const swap_adress = "0x4309D06ea8606798fd3b7C813acB6B0e5d84E08e"
    this.setState({ account: accounts[0] })
    const ethBalance = await web3.eth.getBalance(this.state.account)
    this.setState({ ethBalance })
      const daiToken = new web3.eth.Contract(DaiToken.abi,Dai_adress )
      this.setState({ daiToken })
      let daiTokenBalance = await daiToken.methods.balanceOf(this.state.account).call()
      this.setState({ daiTokenBalance: daiTokenBalance.toString() })
      console.log(daiTokenBalance)

      const novaToken = new web3.eth.Contract(NovaToken.abi, nova_adress)
      this.setState({ novaToken })
      let novaTokenBalance = await novaToken.methods.balanceOf(this.state.account).call()
      this.setState({ novaTokenBalance: novaTokenBalance.toString() })
        
      const ethSwap = new web3.eth.Contract(EthSwap.abi, swap_adress)
      this.setState({ ethSwap })


      const novaFarm = new web3.eth.Contract(NovaFarm.abi, farm_adress)
      this.setState({ novaFarm })
      let stakingBalance = await novaFarm.methods.stakingBalance(this.state.account).call()
      this.setState({ stakingBalance: stakingBalance.toString() })

    this.setState({ loading: false })
  }

  async loadWeb3() {
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum)
      await window.ethereum.enable()
    }
    else if (window.web3) {
      window.web3 = new Web3(window.web3.currentProvider)
    }
    else {
      window.alert('Non-Ethereum browser detected')
    }
  }

  stakeTokens = (amount) => {
    this.setState({ loading: true })
    this.state.daiToken.methods.approve(this.state.novaFarm._address, amount).send({ from: this.state.account }).on('transactionHash', (hash) => {
      this.state.novaFarm.methods.stakeTokens(amount).send({ from: this.state.account }).on('transactionHash', (hash) => {
        this.setState({ loading: false })
      })
    })
  }

  unstakeTokens = (amount) => {
    this.setState({ loading: true })
    this.state.novaFarm.methods.unstakeTokens().send({ from: this.state.account }).on('transactionHash', (hash) => {
      this.setState({ loading: false })
    })
  }
  buyTokens = (etherAmount) => {
    this.setState({ loading: true })
    this.state.ethSwap.methods.buyTokens(etherAmount).send({from: this.state.account }).on('transactionHash', (hash) => {
      this.setState({ loading: false })
    })
  }

  sellTokens = (tokenAmount) => {
    this.setState({ loading: true })
    this.state.daiToken.methods.approve(this.state.ethSwap._address, tokenAmount).send({ from: this.state.account }).on('transactionHash', (hash) => {
      this.state.ethSwap.methods.sellTokens(tokenAmount).send({ from: this.state.account }).on('transactionHash', (hash) => {
        this.setState({ loading: false })
      })
    })
  }

  constructor(props) {
    super(props)
    this.state = {
      account: '0x0',
      daiToken: {},
      novaToken: {},
      novaFarm: {},
      ethSwap: {},
      ethBalance: '0',
      daiTokenBalance: '0',
      novaTokenBalance: '0',
      stakingBalance: '0',
      loading: true
    }
  }

  render() {
    let content
    if(this.state.loading) {
      content = <p id="loader" className="text-center">Loading...</p>
    } else {
      content = <Main
        daiTokenBalance={this.state.daiTokenBalance}
        novaTokenBalance={this.state.novaTokenBalance}
        stakingBalance={this.state.stakingBalance}
        ethBalance={this.state.ethBalance}
        buyTokens={this.buyTokens}
        sellTokens={this.sellTokens}
        stakeTokens={this.stakeTokens}
        unstakeTokens={this.unstakeTokens}
      />
    }

    return (
      <div>
        <Navbar account={this.state.account} />
        <div className="container-fluid mt-5">
          <div className="row">
            <main role="main" className="col-lg-12 ml-auto mr-auto" style={{ maxWidth: '600px' }}>
              <div className="content mr-auto ml-auto">
                <a
                  href="https://novafinance.app/"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                </a>

                {content}

              </div>
            </main>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
