import React, { Component } from 'react'
import dai from '../dai.png'
import sqoin from '../sqoin.png'
import BuyForm from './BuyForm'
import SellForm from './SellForm'
import StakeForm from './stakeForm'
class Main extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentForm: 'buy'
    }
  }
  render() {
    let content
    if(this.state.currentForm === 'buy') {
      content = <BuyForm
        ethBalance={this.props.ethBalance}
        daiTokenBalance={this.props.daiTokenBalance}
        buyTokens={this.props.buyTokens}
      />
    } else if(this.state.currentForm === 'sell') {
      content = <SellForm
        ethBalance={this.props.ethBalance}
        daiTokenBalance={this.props.daiTokenBalance}
        sellTokens={this.props.sellTokens}
      />
    }
    else {
        content = <StakeForm
        ethBalance={this.props.ethBalance}
        daiTokenBalance={this.props.daiTokenBalance}
        sellTokens={this.props.sellTokens}
        stakingBalance={this.props.stakingBalance}
        novaTokenBalance={this.props.novaTokenBalance}
        stakeTokens={this.props.stakeTokens}
        unstakeTokens={this.props.unstakeTokens}

      />
    }
    return (

      <div id="content" className="mt-3">
        <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
<img src={sqoin} alt="logo" style={{ alignSelf: 'center' }} className="w-20 cursor-pointer" />
        </div>
        <div className="d-flex mb-3">
          <button
              className="btn btn-light"
              onClick={(event) => {
                this.setState({ currentForm: 'buy' })
              }}
            >
            Buy
          </button>
          <span className="text-muted">&lt; &gt;</span>
          <button
              className="btn btn-light " 
              onClick={(event) => {
                this.setState({ currentForm: 'sell' })
              }}
            >
            Sell
          </button>
          <button
              className="btn btn-light "
              style={{marginLeft:400}}
              onClick={(event) => {
                this.setState({ currentForm: 'stake' })
              }}
            >
            Stake
          </button>
          
        </div>
        <div className="card mb-4" >

<div className="card-body">

  {content}

</div>

</div>
      </div>
    );
  }
}

export default Main;
